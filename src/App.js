import "./App.css"; 
import Home from "./pages/Home"; 
import Login  from "./pages/Login"; 
import {BrowserRouter,Routes,Route} from "react-router-dom" 
import Navbar from "./components/Navbar"; 
function App() { 
  return( 
    <BrowserRouter> 
    <Routes> 
      <Route path="/" element={<Navbar />}> 
 
         
<Route path="Login" element={<Login/>} /> 
<Route index element={<Home />} /> 
        </Route> 
    </Routes> 
    </BrowserRouter> 
  )  
   
} 
 
export default App;
