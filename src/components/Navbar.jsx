import React from "react"; 
import { Outlet,Link } from "react-router-dom"; 
 
const Navbar = () => { 
  return <div> 
    <Link to='/'>HOME</Link> 
    <Link to='/Login'>LOGIN</Link> 
    <Outlet /> 
  </div>; 
}; 
 
export default Navbar;
